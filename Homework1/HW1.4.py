num1 = int(input("Input first integer number: "))
num2 = int(input("Input second integer number: "))

print("Sum result: ", num1 + num2)
print("Sub result: ", num1 - num2)
print("Mul result: ", num1 * num2)
print("Div result: ", num1 / num2)
print("Remainder after division: ", num1 % num2)

equal_or_greater = num1 >= num2
print(equal_or_greater)
