                                    #First exam
student_1 = [84, "Passed"]
student_2 = [78, "Passed"]
student_3 = [65, "Failed"]
student_4 = [100, "Passed"]
student_5 = [80, "Failed"]

Students_exam_results = [student_1, student_2, student_3, student_4, student_5]
lower_passed_rate = 100
higher_failed_rate = 0

for student in Students_exam_results:
    grade = student[0]
    student_grade = student[1]
    if student_grade == "Failed":
        if grade >= higher_failed_rate:
            higher_failed_rate = grade

for student in Students_exam_results:
    grade = student[0]
    student_grade = student[-1]
    if student_grade == "Passed":
        if grade <= lower_passed_rate:
            lower_passed_rate = grade

is_grubble_consistent = lower_passed_rate >= higher_failed_rate
if is_grubble_consistent:
    print("Prof. Grubble was consistent")
    print(f"Range, for passing the exam: {higher_failed_rate + 1} - {lower_passed_rate} ")
else:
    print("Prof. Grubble was not consistent")
    print(f"{lower_passed_rate} is lower then {higher_failed_rate}")


