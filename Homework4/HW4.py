import time

Numbers = int(input())

start_time = time.time()

Numbers_after_sieve = [i for i in range(Numbers + 1)]


Numbers_after_sieve[1] = 0
i = 2
while i <= Numbers ** 0.5:
    if Numbers_after_sieve[i] != 0:
        j = i ** 2
        while j <= Numbers:
            Numbers_after_sieve[j] = 0
            j = j + i
    i = i + 1

Numbers_after_sieve = [i for i in Numbers_after_sieve if Numbers_after_sieve[i] != 0]

end_time = time.time()

print(f"Прості числа до {Numbers}: {Numbers_after_sieve}")


execution_time = end_time - start_time
print(f"Час виконання: {execution_time:.6f} секунд")