
file_path = 'D:\pyCharm\pythonProject\pythonHomework\homework-zavadskii-valentin\Homework5\orders.txt'  # Вставте шлях до завантаженого файлу

with open(file_path, 'r') as file:
    data = file.read()


orders = data.strip().split('\n\n')


parsed_orders = []
for order in orders:
    products = order.split('@@@')
    parsed_orders.append(products)


for i, order in enumerate(parsed_orders, 1):
    print(f"Order {i}:")
    for j, product in enumerate(order, 1):
        print(f"\tProduct {j}: {product}")
