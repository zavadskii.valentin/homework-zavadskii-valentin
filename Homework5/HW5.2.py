import requests

class OpenWeatherMap:
    def __init__(self, city: str, api_key: str):
        self.__api_key = api_key
        self.__city = city
        self.__data = self.__fetch_data()

    def __fetch_data(self):
        url = f"https://api.openweathermap.org/data/2.5/weather?q={self.__city}&appid={self.__api_key}"
        response = requests.get(url)
        if response.status_code == 200:
            return response.json()
        else:
            raise ValueError(f"Failed to fetch data for city: {self.__city}")

    def get_temp(self) -> float:
        return round(float(self.__data['main']['temp']) - 273.15, 1)

    def get_weather(self) -> str:
        return self.__data['weather'][0]['main']

    def get_wind(self) -> dict:
        return self.__data['wind']

    def get_city(self) -> str:
        return self.__data['name']

    def get_text(self) -> str:
        info = {
            "City": self.get_city(),
            "Temperature (°C)": self.get_temp(),
            "Weather": self.get_weather(),
            "Wind": self.get_wind()
        }
        return "\n".join([f"{key}: {value}" for key, value in info.items()])

    def get_any_key(self, *args):
        result = self.__data
        for key in args:
            result = result.get(key, None)
            if result is None:
                return None
        return result

    def __str__(self):
        return (f"Today weather in {self.get_city()} is {self.get_weather()}. "
                f"Temperature is {self.get_temp()} °C. "
                f"Wind details: {self.get_wind()}.")

def display_weather():
    city = input("Enter city name: ")
    api_key = "b2cf51030cd16f86f1177bff23e22712"
    try:
        weather = OpenWeatherMap(city, api_key)
        print(weather.get_text())
    except ValueError as e:
        print(e)


display_weather()
