import os
from dotenv import load_dotenv

load_dotenv()

TELEGRAM_TOKEN = os.getenv("TELEGRAM_TOKEN")
EXCHANGE_API = os.getenv("EXCHANGE_API")
HISTORY_FILE = "conversion_history.json"