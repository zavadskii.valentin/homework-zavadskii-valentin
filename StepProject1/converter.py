import requests
from config import EXCHANGE_API

def get_exchange_rate(base_currency, target_currency):
    try:
        response = requests.get(EXCHANGE_API, params={"base": base_currency, "symbols": target_currency})
        data = response.json()
        return data["rates"].get(target_currency) if "rates" in data else None
    except Exception as e:
        print(f"Помилка при отриманні курсу: {e}")
        return None