import telebot
from config import TELEGRAM_TOKEN
from converter import get_exchange_rate
from history import save_request_to_file

bot = telebot.TeleBot(TELEGRAM_TOKEN)

@bot.message_handler(commands=['start'])
def send_welcome(message):
    bot.reply_to(message, "Привіт! Введіть суму у форматі `100 UAH`.")

@bot.message_handler(func=lambda message: True)
def ask_target_currency(message):
    try:
        parts = message.text.split()
        if len(parts) != 2:
            bot.reply_to(message, "Формат: `100 UAH`", parse_mode="Markdown")
            return

        amount = float(parts[0])
        base_currency = parts[1].upper()

        message_data = {"amount": amount, "base_currency": base_currency}

        markup = telebot.types.ReplyKeyboardMarkup(row_width=3, one_time_keyboard=True)
        currencies = ["USD", "EUR", "GBP"]
        buttons = [telebot.types.KeyboardButton(currency) for currency in currencies]
        markup.add(*buttons)

        bot.reply_to(message, "Оберіть валюту:", reply_markup=markup)
        bot.register_next_step_handler(message, convert_currency, message_data)

    except ValueError:
        bot.reply_to(message, "Некоректна сума!")

def convert_currency(message, message_data):
    try:
        target_currency = message.text.upper()
        amount = message_data["amount"]
        base_currency = message_data["base_currency"]

        rate = get_exchange_rate(base_currency, target_currency)
        if rate:
            converted_amount = round(amount * rate, 2)
            bot.reply_to(message, f"{amount} {base_currency} = {converted_amount} {target_currency}")

            save_request_to_file({
                "amount": amount,
                "base_currency": base_currency,
                "target_currency": target_currency,
                "converted_amount": converted_amount
            })
        else:
            bot.reply_to(message, "Не вдалося отримати курс валют.")
    except Exception as e:
        bot.reply_to(message, f"Помилка: {e}")