import json
from config import HISTORY_FILE

def save_request_to_file(request_data):
    try:
        try:
            with open(HISTORY_FILE, "r") as file:
                history = json.load(file)
        except (FileNotFoundError, json.JSONDecodeError):
            history = []

        history.append(request_data)
        if len(history) > 10:
            history.pop(0)  

        with open(HISTORY_FILE, "w") as file:
            json.dump(history, file, indent=4)
    except Exception as e:
        print(f"Помилка збереження історії: {e}")
